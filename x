#!/bin/bash
set -e
#Initialize gerrit if gerrit site dir is empty.
#This is necessary when gerrit site is in a volume.
if [ "$1" = '/var/gerrit/gerrit-start.sh' ]; then
  echo $GERRIT_SITE
  ls -A "$GERRIT_SITE"
  echo $GERRIT_SITE
  if [ -z "$(ls -A "$GERRIT_SITE")" ]; then
    echo "First time initialize gerrit..."
    echo "GERRIT_WAR=${GERRIT_WAR}" 
    echo "GERRIT_SITE=${GERRIT_SITE}"
    echo "Running -> java -jar ${GERRIT_WAR} init --batch --no-auto-start -d ${GERRIT_SITE}"
    java -jar "${GERRIT_WAR}" init --batch --no-auto-start -d "${GERRIT_SITE}"
    echo "COMPLETE -> java -jar ${GERRIT_WAR} init --batch --no-auto-start -d ${GERRIT_SITE}"
    cat /var/gerrit/review_site/etc/gerrit.config
    #All git repositories must be removed in order to be recreated at the secondary init below.
    rm -rf "${GERRIT_SITE}/git"
    echo "End of ..First time initialize gerrit..."
  fi


  #Section gerrit
  [ -z "${WEBURL}" ] || git config -f "${GERRIT_SITE}/etc/gerrit.config" gerrit.canonicalWebUrl "${WEBURL}"

  #Section database
  if [ "${DATABASE_TYPE}" = 'postgresql' ]; then
    echo "Database Type ${DATABASE_TYPE}"
    git config -f "${GERRIT_SITE}/etc/gerrit.config" database.type "${DATABASE_TYPE}"
    [ -z "${DB_PORT_5432_TCP_ADDR}" ] || git config -f "${GERRIT_SITE}/etc/gerrit.config" database.hostname "${DB_PORT_5432_TCP_ADDR}"
    [ -z "${DB_PORT_5432_TCP_PORT}" ] || git config -f "${GERRIT_SITE}/etc/gerrit.config" database.port "${DB_PORT_5432_TCP_PORT}"
    [ -z "${DB_ENV_POSTGRES_DB}" ] || git config -f "${GERRIT_SITE}/etc/gerrit.config" database.database "${DB_ENV_POSTGRES_DB}"
    [ -z "${DB_ENV_POSTGRES_USER}" ] || git config -f "${GERRIT_SITE}/etc/gerrit.config" database.username "${DB_ENV_POSTGRES_USER}"
    [ -z "${DB_ENV_POSTGRES_PASSWORD}" ] || git config -f "${GERRIT_SITE}/etc/secure.config" database.password "${DB_ENV_POSTGRES_PASSWORD}"
  fi

  #Section ldap
  echo "Auth Type ${AUTH_TYPE}"
  if [ "${AUTH_TYPE}" = 'LDAP' ]; then
    echo "Auth Type ${AUTH_TYPE}"
    git config -f "${GERRIT_SITE}/etc/gerrit.config" auth.type "${AUTH_TYPE}"
    git config -f "${GERRIT_SITE}/etc/gerrit.config" auth.gitBasicAuth true
    git config -f "${GERRIT_SITE}/etc/gerrit.config" ldap.server "ldap://${LDAP_SERVER}"
    git config -f "${GERRIT_SITE}/etc/gerrit.config" ldap.accountBase "$LDAP_ACCOUNTBASE"
    git config -f "${GERRIT_SITE}/etc/gerrit.config" ldap.accountPattern "$LDAP_ACCOUNTPATTERN"
    git config -f "${GERRIT_SITE}/etc/gerrit.config" ldap.accountFullName "$LDAP_ACCOUNTFULLNAME"
    git config -f "${GERRIT_SITE}/etc/gerrit.config" ldap.accountEmailAddress "$LDAP_ACCOUNTEMAILADDRESS"
    git config -f "${GERRIT_SITE}/etc/gerrit.config" ldap.username "$LDAP_USERNAME"
    git config -f "${GERRIT_SITE}/etc/gerrit.config" ldap.password "$LDAP_PASSWORD"
    git config -f "${GERRIT_SITE}/etc/gerrit.config" ldap.groupBase "${LDAP_GROUPBASE}"
  fi

  # section container
  [ -z "${JAVA_HEAPLIMIT}" ] || git config -f "${GERRIT_SITE}/etc/gerrit.config" container.heapLimit "${JAVA_HEAPLIMIT}"
  [ -z "${JAVA_OPTIONS}" ] || git config -f "${GERRIT_SITE}/etc/gerrit.config" container.javaOptions "${JAVA_OPTIONS}"
  [ -z "${JAVA_SLAVE}" ] || git config -f "${GERRIT_SITE}/etc/gerrit.config" container.slave "${JAVA_SLAVE}"

  #Section sendemail
  if [ -z "${SMTP_SERVER}" ]; then
    git config -f "${GERRIT_SITE}/etc/gerrit.config" sendemail.enable false
  else
    git config -f "${GERRIT_SITE}/etc/gerrit.config" sendemail.smtpServer "${SMTP_SERVER}"
  fi

  #Section plugins
  ls -lastr ${GERRIT_SITE}
  ls -lastr ${GERRIT_SITE}/plugins
  java -jar /var/gerrit/review_site/bin/gerrit.war init --batch --install-plugin download-commands -d ${GERRIT_SITE}
  java -jar /var/gerrit/review_site/bin/gerrit.war init --batch --install-plugin reviewnotes -d ${GERRIT_SITE}
  java -jar /var/gerrit/review_site/bin/gerrit.war init --batch --install-plugin singleusergroup -d ${GERRIT_SITE}
  java -jar /var/gerrit/review_site/bin/gerrit.war init --batch --install-plugin replication -d ${GERRIT_SITE}
  java -jar /var/gerrit/review_site/bin/gerrit.war init --batch --install-plugin commit-message-length-validator -d ${GERRIT_SITE}
  curl -fsSL http://xvxax500/pub/files/gerrit-plugins/delete-project.jar -o ${GERRIT_SITE}/plugins/delete-project.jar
  curl -fsSL http://xvxax500/pub/files/gerrit-plugins/admin-console.jar -o ${GERRIT_SITE}/plugins/admin-console.jar
  java -jar ${GERRIT_SITE}/bin/gerrit.war init --batch --install-plugin delete-project -d ${GERRIT_SITE}
  java -jar ${GERRIT_SITE}/bin/gerrit.war init --batch --install-plugin admin-console -d ${GERRIT_SITE}
  git config -f "${GERRIT_SITE}/etc/gerrit.config" plugins.allowRemoteAdmin true
  git config -f "${GERRIT_SITE}/etc/gerrit.config" gitweb.cgi /var/www/git/gitweb.cgi
  git config -f "${GERRIT_SITE}/etc/gerrit.config" httpd.listenUrl proxy-http://*:8080/gerrit/

  echo "Upgrading gerrit..."
  java -jar "${GERRIT_WAR}" init --batch -d "${GERRIT_SITE}"
  if [ $? -eq 0 ]; then
    echo "Upgrading is OK."
  else
    echo "Something wrong..."
    cat "${GERRIT_SITE}/logs/error_log"
  fi
  ls /var/gerrit/review_site/plugins
  cat /var/gerrit/review_site/etc/gerrit.config
fi
exec "$@"

